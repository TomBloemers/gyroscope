\documentclass[10pt,final,journal,oneside,twocolumn,a4paper]{IEEEtran}

% Custom package includes
\usepackage[pdftex]{graphicx}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{float}
\usepackage{cite}
\usepackage{color}

\newcommand*\Eval[3]{\left.#1\right\rvert_{#2}^{#3}}

\begin{document}

\title{Gyroscope Dynamics Manual}

%\author{Tom~Bloemers}%
% make the title area
\maketitle

% The paper headers
\markboth{Systems~and~Control Integration Project, Gyroscope Dynamics Manual, V2021}%
{Gyroscope Dynamics Manual, V2021}

% As a general rule, do not put math, special symbols or citations
% in the abstract or keywords.
%\begin{abstract}
%The abstract goes here.
%\end{abstract}

\section{INTRODUCTION}
In this document, a derivation of the dynamics describing the Quanser 3-DOF gyroscope is explained. First an overview of the system is presented and based on this, a set of generalized coordinate frames is defined. Through the Euler-Lagrange equations and the defined frames a dynamical model of the system is derived. Finally, a convenient way to compute the inertia and Coriolis matrices are presented.

For compactness and readability, sinusoidal functions are abbreviated as $s_i$ and $c_i$, e.g., $\sin q_2 = s_2$  and $\cos^2q_3 = c^2_3$. We refer the interested user to \cite{robotics_book} for more information on modeling of multi-body systems.
%----------------------------------------------------------------------------- DYNAMICS -----------------------------------------------------------------------------
\section{DYNAMICS}
The 3-DOF gyroscope consists of three gimbals ($A$, $B$ and $C$) along with a symmetric disk ($D$) as shown in Figure \ref{fig:Gyro_Schematic}. Right orientated sets of orthogonal unit vectors $e_i^j$ with $i = x,y,z$ and $j=A,B,C,D,N$ are fixed to the natural (i.e., world) reference frame $j = N$ and the bodies ($A$, $B$, $C$ and $D$) respectively. The origins of the coordinate frames are located in the centre of the disk ($D$).
\begin{figure}[H]
\centering
\includegraphics[width=1.00\linewidth]{Figures/Gyro_Schematic.pdf}
\caption{Geometrical description of the 3DOF set-up. The torque directions are indicated by $\tau_i$. To distinguish the coordinate frames, the unit vectors $\vec{e}^j_i$ have been drawn separately in different positions, but the origin of each frame is located in the center of $D$. }
\label{fig:Gyro_Schematic}
\end{figure}
Define the generalized angular position vector $q \in \mathbb{R}^{n_q}$, the generalized angular velocity vector $\dot{q} \in \mathbb{R}^{n_q}$ and the generalized angular acceleration vector $\ddot{q} \in \mathbb{R}^{n_q}$ as:
\begin{subequations}
\begin{align}
q &:= \begin{bmatrix} q_1 & q_2 & q_3 & q_4 \end{bmatrix}^\top, \\
\dot{q} &:= \begin{bmatrix} \dot{q}_1 & \dot{q}_2 & \dot{q}_3 & \dot{q}_4 \end{bmatrix}^\top, \\
\ddot{q} &:= \begin{bmatrix} \ddot{q}_1 & \ddot{q}_2 & \ddot{q}_3 & \ddot{q}_4 \end{bmatrix}^\top.
\end{align}
\end{subequations}
Note that $q_1$ is connected to the disk $D$, $q_2$ to gimbal $C$, $q_3$ to gimbal $B$ and $q_4$ to gimbal $A$. Define the input torque vector $\tau \in \mathbb{R}^{n_\tau}$ as
\begin{align}
\tau := \begin{bmatrix} \tau_1 & \tau_2 & \tau_3 & \tau_4 \end{bmatrix}^\top,
\end{align}
where the torques $\tau_i$ ($i = 1,2,3$ and $4$) can be exerted on the frames ($D$, $C$, $B$ and $A$) respectively. The positive rotation direction of each frame is defined as
\begin{itemize}
\item $q_1$: angle rotation around the $e^D_y$-axis w.r.t. frame C,
\item $q_2$: angle rotation around the $e^C_x$-axis w.r.t. frame B,
\item $q_3$: angle rotation around the $e^B_y$-axis w.r.t. frame A,
\item $q_4$: angle rotation around the $e^A_z$-axis w.r.t. frame N.
\end{itemize}
Furthermore, let the frame orientation indicated in Figure \ref{fig:Gyro_Schematic} also indicate the initial position. For convenience, we define the body frame set $\mathcal{S}$:
\begin{align}
\mathcal{S} := \{ A,B,C,D\}.
\end{align}

The center of mass for all bodies comprising the system are assumed to be at the center of the disk ($D$). Thus, only rotational dynamics are considered and rectilinear dynamics and gravity effects are neglected. The principal inertia tensors $\mathcal{I}^k_k$ for frame $k$ w.r.t. the same frame $k$, with $k \in \mathcal{S}$, are given as
\begin{align}
\label{eqn:inertia_tensors}
\begin{aligned}
\mathcal{I}^A_A &= \begin{bmatrix} I_A & 0 & 0 \\ 0 & J_A & 0 \\ 0 & 0 & K_A \end{bmatrix}, \\
\mathcal{I}^C_C &= \begin{bmatrix} I_C & 0 & 0 \\ 0 & J_C & 0 \\ 0 & 0 & K_C \end{bmatrix}, 
\end{aligned} & & 
\begin{aligned}
\mathcal{I}^B_B &= \begin{bmatrix} I_B & 0 & 0 \\ 0 & J_B & 0 \\ 0 & 0 & K_B \end{bmatrix}, \\
\mathcal{I}^D_D &= \begin{bmatrix} I_D & 0 & 0 \\ 0 & J_D & 0 \\ 0 & 0 & I_D \end{bmatrix}, 
\end{aligned}
\end{align} 
where the $I_k$, $J_k$, $K_k$ ($k \in \mathcal{S}$) elements are the scalar moments of inertia about the $i^\mathrm{th}$ ($i=$ $x$, $y$, $z$) axis respectively in the bodies $\mathcal{S}$. Note that the inertia tensor $\mathcal{I}^D_D$ is simplified due to symmetry in the disk.

Let the rotation matrices $R^i_j = \begin{bmatrix} X_j^i & Y_j^i & Z_j^i \end{bmatrix}$ denote the coordinate transformation from inertial frame $j$ to $i$, which can be constructed from the Cartesian rotation matrices:
\begin{subequations}
\begin{align}
R_X(q_j) = \begin{bmatrix} 1 & 0 & 0 \\ 0 & \cos q_j & -\sin q_j \\ 0 & \sin q_j & \cos q_j \end{bmatrix}, \\
R_Y(q_j) = \begin{bmatrix} \cos q_j & 0 & \sin q_j \\ 0 & 1 & 0 \\ -\sin q_j & 0 &  \cos q_j \end{bmatrix}, \\
R_Z(q_j) = \begin{bmatrix} \cos q_j & -\sin q_j & 0 \\ \sin q_j & \cos q_j & 0 \\ 0 & 0 & 1 \end{bmatrix},
\end{align}
\end{subequations}
where 
\begin{align}
\label{eqn:rotation_matrices}
\begin{aligned}
R^N_A &= R_Z(q_4), \\
R^B_C &= R_X(q_2), 
\end{aligned} & & 
\begin{aligned}
R^A_B = R_Y(q_3), \\
R^C_D = R_Y(q_1).  
\end{aligned}
\end{align}
Utilizing these rotation matrices, one can for instance describe the frame rotation between the reference frame $N$ and the disk frame $D$ as
\begin{align*}
R^N_D = R^N_A R^A_B R^B_C R^C_D.
\end{align*} 

The rotation matrices \eqref{eqn:rotation_matrices} are utilized to relate the relative angular velocities to the generalized angular velocities as observed from frame $N$. For example, the relative velocity between frame $A$ and $B$ as observed from frame $N$ is given as
\begin{align}
\omega^N_{A,B} = R^N_A\vec{e}_y\dot{q}_3 = Y^N_A\dot{q}_3
\end{align}
where $Y^N_A$ is the unit vector in the $y$-direction of frame $A$ observed from frame $N$. The relative velocity of each frame observed through frame $N$ is equal to the sum of the relative angular velocities of the connected inertial frames.
\begin{subequations}
\label{eqn:angular_velocities}
\begin{align}
\omega^N_{N,A} &= \begin{bmatrix} 0 & 0 & 0 & Z^N_N \end{bmatrix} \dot{q}, \\
\omega^N_{N,B} &= \begin{bmatrix} 0 & 0 & Y^N_A & Z^N_N\end{bmatrix} \dot{q}, \\
\omega^N_{N,C} &= \begin{bmatrix} 0 & X^N_B & Y^N_A & Z^N_N\end{bmatrix} \dot{q}, \\
\omega^N_{N,D} &= \begin{bmatrix} Y^N_C & X^N_B & Y^N_A & Z^N_N\end{bmatrix} \dot{q},  
\end{align}
\end{subequations}
where 
\begin{align}
\begin{aligned}
Z^N_N &= \vec{e}_z, \\
X^N_B &= R^N_B \vec{e}_x, 
\end{aligned} & & 
\begin{aligned}
Y^N_A &= R^N_A \vec{e}_y, \\
Y^N_C &= R^N_C \vec{e}_y.
\end{aligned}
\end{align}
The angular velocities \eqref{eqn:angular_velocities} are written in the form $\omega^N_{N,k} = J^N_{N,k}\dot{q}$, where $J^N_{N,k}$ is the Jacobian that relates the angular velocities $\omega^N_{N,k}$ to the generalized angular velocities $\dot{q}$. Observe that the Jacobian is a function of the generalized angular positions, e.g., $J^N_{N,k}(q)$.

The kinetic energy of the system can be described as the sum of the kinetic energies of each body comprising the system. Utilizing the principal inertia tensors, rotation matrices and the Jacobians, the kinetic energy is
\begin{align}
\label{eqn:kinetic_energy}
T(q,\dot{q}) = \frac{1}{2}\sum_{k \in \mathcal{S}} \dot{q}^\top \left[ \left( J^N_{N,k}\right)^\top  R^N_{k} \mathcal{I}_k^k \left( R^N_{k}\right)^\top  J^N_{N,k} \right] \dot{q}.
\end{align}
As it is assumed that gravity effects are neglected there is no potential energy. In addition, viscous friction can be captured by means of the Rayleigh dissipation function:
\begin{equation}
	\mathcal{D}(\dot{q}) = \frac{1}{2}\dot{q}^\top D \dot{q},
\end{equation}
where $D$ containts constant terms relating to the damping coefficients in the physical system. Consequently, the Lagrangian is equal to the kinetic energy \eqref{eqn:kinetic_energy}
\begin{align}
L(q,\dot{q}) = T(q,\dot{q}).
\end{align}
The equations of motion in the generalized coordinates $q$ are derived using the Euler-Lagrange equations 
\begin{align}
\frac{d}{dt} \frac{\partial L(q,\dot{q})}{\partial \dot{q}_i} - \frac{\partial L(q,\dot{q})}{\partial q_i} = \tau_i - \frac{\partial \mathcal{D}(\dot{q})}{\partial \dot{q}_i}\quad i = 1,\dots,n_q
\end{align}
and can be written as a set of second order differential equations
% ------ Dynamic equations ------
\begin{align}
\sum_{j=1}^{n_q}M_{ij}(q)\ddot{q}_j + \sum_{j=1}^{n_q}\sum_{k=1}^{n_q}\Gamma_{ijk}\dot{q}_j \dot{q}_k &= \tau_i - \sum_{j=1}^{n_q} D_{ij}\dot{q}_j,
\end{align}
for $i = 1,\dots,n_q$, or equivalently for $i = 1,\dots,n_q$:
\begin{align}
\label{eqn:ELeq_summ}
\sum_{j=1}^{n_q}M_{ij}(q)\ddot{q}_j + \sum_{j=1}^{n_q} C_{ij}(q,\dot{q})\dot{q}_j &= \tau_i - \sum_{j=1}^{n_q} D_{ij}\dot{q}_j.
\end{align}
Here $M_{ij}(q)$ denotes the $i^\mathrm{th}$ and $j^\mathrm{th}$ row and column index of $M(q)$ respectively. The inertia matrix $M(q)$can be computed as
\begin{align}
M(q) = \sum_{k \in \mathcal{S}} \left( J^N_{N,k}\right)^\top  R^N_{k} \mathcal{I}_k^k \left( R^N_{k}\right)^\top  J^N_{N,k}.
\end{align}
The elements of the Coriolis matrix $C(q,\dot{q})$ are the result of the summation of the Christoffel symbols and the generalized angular velocities 
\begin{align}
\label{eqn:coriolis_matrix}
C_{ij}(q,\dot{q}) = \sum_{k=1}^{n_q} \Gamma_{ijk}\dot{q}_k, \quad i,j = 1,\dots,n_q
\end{align}
where the Christoffel symbols $\Gamma_{ijk}$ corresponding to the inertia matrix $M(q)$ can be chosen as
\begin{align}
\Gamma_{ijk} = \frac{1}{2} \left( \frac{\partial M_{ij}(q)}{\partial q_k} + \frac{\partial M_{ik}(q)}{\partial q_j} - \frac{\partial M_{kj}(q)}{\partial q_i} \right)
\end{align}

It is interesting to note that
\begin{itemize}
\item $\Gamma_{ijk} = \Gamma_{ikj}$.
\item Terms of the form $\dot{q}_i \dot{q}_j$, $i\neq j$ are the Coriolis forces and terms of the form $\dot{q}_i \dot{q}_j$, $i = j$ are the Centrifugal forces.
\item There are other ways to define the matrix $C(q,\dot{q})$ such that $C_{ij}(q,\dot{q})\dot{q}_j = \Gamma_{ijk}\dot{q}_j\dot{q}_k$. 
\item Rewriting the equations \eqref{eqn:ELeq_summ} into a vectorized form gives the well-known result:
\begin{equation} 
M(q)\ddot{q} + C(q,\dot{q})\dot{q} + D\dot{q} = \tau.
\end{equation}
\end{itemize}

The inertia matrix $M(q)$, the Christoffel symbols $\Gamma_{ijk}$ for the Coriolis matrix $C(q,\dot{q})$ and the dissipation matrix $D$ can be found in Appendix \ref{apx:A}.

\appendices
\section{INERTIA AND CORIOLIS MATRICES}
\label{apx:A}
The inertia matrix $M(q)$ is given as the summation of the inertia matrices for each frame in $\mathcal{S}$:
\begin{align}
M(q) = \sum_{k \in \mathcal{S}} M_k(q)
\end{align}
%Ma
\footnotesize%
\begin{align*}
M_A &= 
\begin{bmatrix}
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & K_A 
\end{bmatrix} \\
%Mb
M_B &= 
\begin{bmatrix}
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & J_B & 0 \\
0 & 0 & 0 & I_Bs^2_3 + K_Bc^2_3 
\end{bmatrix} \\
%Mc
M_C &= 
\begin{bmatrix}
0 & 0 & 0 & 0 \\
0 & I_C & 0 & -I_Cs_3 \\
0 & 0 & J_Cc^2_2+K_Cs^2_2 & \alpha_1s_2c_2c_3 \\
0 & -I_Cs_3 & \alpha_1s_2c_2c_3 & I_Cs^2_3+(J_Cs^2_2+K_Cc_2^2)c^2_3 
\end{bmatrix} \\
%Md
M_D &= 
\begin{bmatrix}
J_D & 0 & J_Dc_2 & J_Ds_2c_3 \\
0 & I_D & 0 & -I_Ds_3 \\
J_Dc_2 & 0 & I_Ds^2_2+J_Dc^2_2 & \alpha_2s_2c_2c_3 \\
J_Ds_2c_3 & -I_Ds_3 & \alpha_2s_2c_2c_3 & I_Ds^2_3+(I_Dc^2_2+J_Ds^2_2)c^2_3 
\end{bmatrix}
\end{align*}
\normalsize

The elements of the Coriolis matrix $C(q,\dot{q})$ can be computed as the summation of the Christoffel symbols and the generalized angular velocities as in \eqref{eqn:coriolis_matrix}. The Christoffel symbols $\Gamma_{ijk}$ are collected in matrices for varying $i$, these matrices are denoted as $\Gamma^i$. For example, the Christoffel symbol $\Gamma_{4,3,2}$ can be found in the 4th matrix $\Gamma^4$ with row index number $j = 3$ and column index number $k = 2$, i.e., $\Gamma_{3,2}^4 = \alpha_3(c_3s^2_2-c^2_2c_3)-\alpha_4c_3$.

%% Gamma for i = 1,2,3,4
% i = 1
\footnotesize
\begin{align*}
\Gamma^1 &= \frac{1}{2}
\begin{bmatrix}
0 & 0 & 0 & 0 \\
\star & 0 & -J_Ds_2 & J_Dc_2c_3 \\
\star & \star & 0 & -J_Ds_2s_3 \\
\star & \star & \star & 0
\end{bmatrix} \\
% i = 2
\Gamma^2 &= \frac{1}{2}
\begin{bmatrix}
0 & 0 & J_Ds_2 & -J_Dc_2c_3 \\
\star & 0 & 0 & 0 \\
\star & \star & -2\alpha_3s_2c_2 & \alpha_3(c^2_2c_3-s^2_2c_3)-\alpha_4c_3 \\
\star & \star & \star & 2\alpha_3c_2c^2_3s_2
\end{bmatrix} \\
% i = 3
\Gamma^3 &= \frac{1}{2}
\begin{bmatrix}
0 & -J_Ds_2 & 0 & J_Ds_2s_3 \\
\star & 0 & 2\alpha_3s_2c_2 & \alpha_4c_3 + \alpha_3(c_3s^2_2-c^2_2c_3) \\
\star & \star & 0 & 0 \\
\star & \star & \star & -2(\alpha_5+\alpha_3s^2_2)c_3s_3
\end{bmatrix} \\
% i = 4
\Gamma^4 &= \frac{1}{2}
\begin{bmatrix}
0 & J_Dc_2c_3 & -J_Ds_2s_3 & 0 \\
\star & 0 & \alpha_3(c_3s^2_2-c^2_2c_3)-\alpha_4c_3 & -2\alpha_3c_2c^2_3s_2 \\
\star & \star & 2\alpha_3c_2s_2s_3 & 2(\alpha_5+\alpha_3s^2_2)c_3s_3 \\
\star & \star & \star & 0
\end{bmatrix}
\end{align*}
\normalsize
using the property that $\Gamma_{ijk} = \Gamma_{ikj}$, the $\star$ symbol denotes terms required to make the matrix symmetric (i.e., they correspond to the transpose of their upper triangular pair). The variables $\alpha_i$ are defined as:
\begin{align*}
\alpha_1 &:= J_C-K_C \\
\alpha_2 &:= J_D-I_D \\
\alpha_3 &:= I_D-J_C-J_D+K_C \\
\alpha_4 &:= I_C+I_D \\
\alpha_5 &:= I_B+I_C-K_B-K_C. 
\end{align*}
Vectorizing the computation of \eqref{eqn:coriolis_matrix} gives:
\begin{align*}
C(q,\dot{q}) = \begin{bmatrix}
\dot{q}^\top & 0 & 0 & 0 \\
0 & \dot{q}^\top & 0 & 0 \\
0 & 0 & \dot{q}^\top & 0 \\
0 & 0 & 0 & \dot{q}^\top
\end{bmatrix}
\begin{bmatrix}
\Gamma^1 \\ \Gamma^2 \\ \Gamma^3 \\ \Gamma^4
\end{bmatrix}.
\end{align*}
The matrix $D$ containing the viscous frictions is assumed diagonal:
\begin{equation}
	D = \begin{bmatrix}
		d_1 & 0 & 0 & 0 \\ 0 & d_2 & 0 & 0 \\ 0 & 0 & d_3 & 0 \\ 0 & 0 & 0 & d_4
	\end{bmatrix},
\end{equation}
with $d_i \geq 0$ the viscous friction terms.

%\section{INERTIA TENSORS}
%\label{apx:B}
%The values of the inertia tensors as defined in \eqref{eqn:inertia_tensors} are given as:
%\begin{align*}
%\mathcal{I}^A_A &= \begin{bmatrix} 0.0902 & 0 & 0 \\ 0 & 0.0534 & 0 \\ 0 & 0 & 0.0375 \end{bmatrix}, \\
%\mathcal{I}^B_B &= \begin{bmatrix} 0.0037 & 0 & 0 \\ 0 & 0.0179 & 0 \\ 0 & 0 & 0.0213 \end{bmatrix}, \\
%\mathcal{I}^C_C &= \begin{bmatrix} 0.0010 & 0 & 0 \\ 0 & 0.0018 & 0 \\ 0 & 0 & 0.0027 \end{bmatrix}, \\
%\mathcal{I}^D_D &= \begin{bmatrix} 0.0028 & 0 & 0 \\ 0 & 0.0056 & 0 \\ 0 & 0 & 0.0028 \end{bmatrix}.
%\end{align*}
%These values are taken from the 3D drawings (see \cite{inertias}) and should be experimentally verified and adjusted by the user if needed.

\bibliography{Bibliography}{}
\bibliographystyle{IEEEtran}

\end{document}



























