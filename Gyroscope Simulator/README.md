# Gyroscope Simulator
This folder contains a Simulink model of the setup (equations implemented according to the Gyroscope Dynamics Manual). Besides an implementation of the  dynamics, a visualization tool of the setup is provided in the Simulink model using the "Simulink 3D Animation Toolbox".

To use the Simulator a Matlab Executable (mex) file is required. Mex files for the macOS and Windows operating systems are provided and you do not have to compile your own. If you are running a Linux based operating system you will have to compile the provided C++ code to a Matlab Executable file yourself.

## Compiling a Matlab Executable file
1. Make sure you have a compatable C++ compiler installed. For windows the MinGW-w64 C/C++ Compiler is available from the add-ons menu.
2. Set up the mex compiler using 
```matlab
>> mex -setup
```
3. Compile a Matlab Executable file by running the following Matlab command:
```matlab
>> mex gyro_simulation.cpp
```

## Parameters
By default, the parameter provided in this directory are loaded (in the initialization callback function). The parameters contain inertias, viscous friction coefficients and motor constants. The viscous friction parameters provide a damping force that is linear w.r.t. the velocities of the gyroscope, i.e., $`F = f_v\dot{q}}`$. The motor constants provide a current-to-torque mapping of the form $`\tau = K_mI`$.

In case no friction is desired, simply set the corresponding parameters (`params.fv1 - params.fv4`) to 0.

In case a current input is desired, set the motor constants (`params.Km1 - params.Km4`) to 1.