# Control Moment Gyroscope

## Gyroscope Dynamics Manual
The folder `Gyroscope Dynamics Manual` provides a derivation of the dymics describing the 3 degree of freedom control moment gyroscope.

## Gyroscope Dynamics
The folder `Gyroscope Dynamics` provides a Matlab script that implements a symbolic derivation of the equations of motion according to the `Gyroscope Dynamics Manual`. 

## Simulation Files
The folder `Gyroscope Simulator` contains a Simulink model of the setup. Besides a implementation of the dynamics, a visualization tool of the setup is provided in the Simulink model using the "Simulink 3D Animation Toolbox". See also the README file in this folder.

## Physical parameters
The file `params.mat` contains (identified) physical parameters of the setup, according to the equations provided in the `Gyroscope Dynamics Manual`. Additional to the inertias described in the `Gyroscope Dynamics Manual`, viscous friction and motor constants are provided. The viscous friction parameters provide a damping force that is linear w.r.t. the velocities of the gyroscope, i.e., $`F = f_v\dot{q}`$. The motor constants provide a current-to-torque mapping of the form $`\tau = K_mI`$.

